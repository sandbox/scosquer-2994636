# Entity Import Export

## Dev environment setup

Deploy Drupal container

```
docker-compose up -d
```

Access Drupal instance
 
[http://localhost:8080]()

During initial Drupal setup

- Language : **English** (avoids translation file download)
- Profile : **Standard**
- Database type : **SQLite**
- Site name : **Site**
- Username : **admin**
- Password : **admin**

## Contributors

- Stefan Cosquer