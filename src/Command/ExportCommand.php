<?php

namespace Drupal\entity_import_export\Command;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\user\Entity\User;
use Drupal\file\Entity\File;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Annotations\DrupalCommand;

/**
 * Class ExportCommand.
 *
 * @package Drupal\entity_import_export
 *
 * @DrupalCommand (
 *     extension="entity_import_export",
 *     extensionType="module"
 * )
 */
class ExportCommand extends Command {

  use ContainerAwareCommandTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('entity:export')
      ->setDescription('Export contents and entities')
      ->setAliases(['eex']);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $serializer = $this->container->get('serializer');
    $file_usage = $this->container->get('file.usage');
    $file_system = $this->container->get('file_system');
    $path = $file_system->realpath('..').'/content/sync';
    mkdir($path, 0755, TRUE);

    $definitions = $this->container->get('entity_type.manager')->getDefinitions();

    foreach ($definitions as $definition) {
      $class = $definition->getClass();
      if ($definition->entityClassImplements(ContentEntityBase::class)) {
        $ids = \Drupal::entityQuery($definition->id())->execute();
        $entities = $class::loadMultiple($ids);

        $io->info('Exporting ' . count($ids) . ' ' . $definition->id(), FALSE);

        foreach ($entities as $entity) {
          if ($class === User::class && $entity->id() <= 1) {
            continue;
          }
          else if ($class === File::class && count($file_usage->listUsage($entity)) > 0) {
            file_unmanaged_copy($entity->getFileUri(), $path . '/' . $definition->id() . '-' . $entity->id());
          }
          $data = $serializer->serialize($entity, 'json', ['simpleref' => TRUE]);
          $data = preg_replace('/,"target_uuid":"[^"]*+"/', '', $data);
          file_put_contents($path . '/' . $definition->id() . '-' . $entity->id() . '.json', $data);
          $io->info('.', FALSE);
        }
        $io->successLite('Ok');
      }
    }
    $io->info('Export successful !');
  }
}
