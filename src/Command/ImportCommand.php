<?php

namespace Drupal\entity_import_export\Command;

use Drupal\Core\Entity\ContentEntityBase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\file\Entity\File;

/**
 * Class ImportCommand.
 *
 * @package Drupal\entity_import_export
 *
 * @DrupalCommand (
 *     extension="entity_import_export",
 *     extensionType="module"
 * )
 */
class ImportCommand extends Command {

  use ContainerAwareCommandTrait;

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('entity:import')
      ->setDescription('Import contents and entities')
      ->setAliases(['eim']);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $serializer = $this->container->get('serializer');
    $file_system = $this->container->get('file_system');
    $path = $file_system->realpath('..') . '/content/sync';

    $definitions = $this->container->get('entity_type.manager')->getDefinitions();

    foreach ($definitions as $definition) {
      $class = $definition->getClass();
      if ($definition->entityClassImplements(ContentEntityBase::class)) {
        $io->info('Importing ' . $definition->id(), FALSE);

        foreach (glob($path . "/" . $definition->id() . "-*.json") as $file) {
          $data = file_get_contents($file);

          try {
            $entity = $serializer->deserialize($data, $class, 'json', ['request_method' => 'POST']);
            if ($class === File::class) {
              $dirname = $file_system->dirname($entity->getFileUri());
              file_prepare_directory($dirname, FILE_CREATE_DIRECTORY);
              file_unmanaged_copy($path . '/' . $definition->id() . '-' . $entity->id(), $entity->getFileUri(), FILE_EXISTS_REPLACE);
            }
            $entity->enforceIsNew(TRUE);
            $entity->save();
          } catch (\Exception $e) {
            $io->errorLite('Error while importing ' . $file. ': ' . $e->getMessage());
          }

          $io->info('.', FALSE);
        }
        $io->info('');
        $io->successLite('Ok');
      }
    }
    $io->successLite('Import successful !');
  }
}
